@extends('layouts.template')

@section('header')
    @include('layouts.header')
@endsection

@section('c')
    @yield('content')
@endsection

@section('footer')
    @include('layouts.footer')
@endsection