<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Country;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    use RegistersUsers;

    protected $redirectTo = '/home';

    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'firstName' => 'required|max:255',
            'lastName' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'country' => 'required|integer|exists:countries,id',
            'company' => 'required|max:100'
        ]);
    }

    protected function create(array $data)
    {
        return User::create([
            'firstName' => $data['firstName'],
            'lastName' => $data['lastName'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'company' => $data['company'],
            'country_id' => $data['country']
        ]);
    }

    public function showRegistrationForm()
    {
        $countries = Country::get(array('id', 'name'));

        return view('auth.register')->with('countries', $countries);
    }
}
