<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = ['firstName', 'lastName', 'email', 'password', 'company', 'country_id'];

    protected $hidden = ['password', 'remember_token'];

    public function todos()
    {
        return $this->hasMany('App\Todo');
    }

    public function country()
	{
		return $this->belongsTo('App\Country');
	}
}
