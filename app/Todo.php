<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Todo extends Model
{
    protected $fillable = ['name', 'completed'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
